import jwaax.exceptions
import requests
import urllib.parse as urlps
from collections import namedtuple

RealmInfo = namedtuple("RealmInfo", "idp_exists redirect_url")


def get_realm_info(realm):
    try:
        url = urlps.urlunparse(urlps.ParseResult(scheme='https', netloc=realm,
                                                 path='/.well-known/jwaax/info.json', params='', query='', fragment=''))
        r = requests.get(url)
    except Exception as e:
        raise jwaax.exceptions.InvalidRealmError() from e
    if r.status_code == 200:
        idpinfo = r.json()
        if not ('idp_realms' in idpinfo and realm in idpinfo['idp_realms']):
            raise jwaax.exceptions.InvalidRealmError()
        idpinfo = idpinfo['idp_realms'][realm]
        return RealmInfo(idp_exists=True, redirect_url=idpinfo['user_redirect'])
    raise jwaax.exceptions.InvalidRealmError()
