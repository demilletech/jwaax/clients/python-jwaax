class Error(Exception):
    pass


class InvalidRealmError(Error):
    pass
