import setuptools
import json
import jmespath

install_requires = []
with open('Pipfile.lock') as f:
    context = json.loads(f.read)
    install_requires.extend(map(
        lambda n, v: n + v,
        jmespath.search('default | keys(@)', context),
        jmespath.search('default.*.version', context),
    ))

long_desc = ''
with open('README.rst') as f:
    long_desc = f.read()

setuptools.setup(
    name='JWAAX',
    use_scm_version=True,
    packages=['jwaax'],
    setup_requires=['jmespath', 'setuptools_scm'],
    install_requires=install_requires,
    url='https://gitlab.com/demilletech/jwaax/clients/python-jwaax',
    author='demilleTech LLC',
    license='MIT',
    description='The demilleTech reference implementation of JWAAX.',
    classifiers=[
        'Programming Language :: Python :: 3 :: Only',
        'Development Status :: 2 - Pre-Alpha',
        'Envrionment :: Plugins',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Security',
        'Topic :: Software Development :: Libraries'
    ]
)
