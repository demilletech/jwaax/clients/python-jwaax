python-jwaax
============

This is the reference Python implementation of JWAAX. For the sake of stability, please use this library, even in other libraries using JWAAX.

Installation
------------
| To install, run :code:`pip install JWAAX`
| The package will now be importable as :code:`jwaax`.